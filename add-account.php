<?php
session_start();
error_reporting(0);
include('includes/dbconnection.php');
if (strlen($_SESSION['brmsaid']==0)) {
  header('location:logout.php');
  } else{
    if(isset($_POST['submit']))
  {

$brmsaid=$_SESSION['brmsaid'];
$name=$_POST['name'];
$PanStatus=$_POST['PanStatus'];
$PanNo=$_POST['PanNo'];
$PanForm=$_POST['PanForm'];
$FName=$_POST['FName'];
$caste=$_POST['caste'];
$MName=$_POST['MName'];
$MStatus=$_POST['MStatus'];
$SName=$_POST['SName'];
$DOB=date('Y-m-d', strtotime($_POST['DOB']));


$NDOB=date('Y-m-d', strtotime($_POST['NDOB']));

$AccountOpeningDate=date('Y-m-d', strtotime($_POST['AccountOpeningDate']));
$DebitAllot=$_POST['DebitAllot'];
$PassbookAllot=$_POST['PassbookAllot'];
$Gender=$_POST['Gender'];
$NName=$_POST['NName'];
$NAddress=$_POST['NAddress'];
$mobnum=$_POST['mobilenumber'];
$address=$_POST['address'];
$cid=$_POST['cid'];
$pnr=$_POST['pnr'];
$AadharNo=$_POST['AadharNo'];
$AcNo=$_POST['AcNo'];
$propic=$_FILES["propic"]["name"];
$sigpic=$_FILES["sigpic"]["name"];
$extension = substr($propic,strlen($propic)-4,strlen($propic));
$extensionsig = substr($sigpic,strlen($sigpic)-4,strlen($sigpic));
$allowed_extensions = array(".jpg","jpeg",".png",".gif");
if(!in_array($extension,$allowed_extensions) && !in_array($extensionsig,$allowed_extensions) )
{
echo "<script>alert('Profile Pics has Invalid format. Only jpg / jpeg/ png /gif format allowed');</script>";
}
else
{

$propic=md5($propic).time().$extension;
$sigpic=md5($sigpic).time().$extension;



$check_select = mysqli_query($conn,"SELECT * FROM tblperson WHERE AccountNo = '$AcNo'"); 

$numrows=mysqli_num_rows($check_select);

if($numrows > 0){
    echo "<script>alert('Account Already exist');</script>";
}

else{

  $sql="insert into tblperson (PNR,CID,AccountNo,AadharNo,Name,FName,MName,MStatus,SName,DOB,Gender,PanStatus,PanNo,PanForm,caste,MobileNumber,Address,NName,NDOB,NAddress,Picture,Signature,DebitAllot,PassbookAllot,AccountOpeningDate) VALUES ('$pnr','$cid','$AcNo','$AadharNo','$name','$FName','$MName','$MStatus','$SName','$DOB','$Gender','$PanStatus','$PanNo','$PanForm','$caste','$mobnum','$address','$NName','$NDOB','$NAddress','$propic','$sigpic','$DebitAllot','$PassbookAllot','$AccountOpeningDate')";
                      
  if($conn->query($sql)===TRUE){
    echo '<script>alert("Person Detail has been added")</script>';
    move_uploaded_file($_FILES["propic"]["tmp_name"],"../images/".$propic);
    move_uploaded_file($_FILES["sigpic"]["tmp_name"],"../images/".$sigpic);
   
}
else
{
echo "Error: " . $sql . "<br>" . $conn->error;
}

}
 
}
}
?>
<!DOCTYPE html>
<html>
<head>
  
  <title>BRMS | Banking Resource Management System</title>
    
  <!-- Font Awesome -->
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <?php include_once('includes/header.php');?>

 
<?php include_once('includes/sidebar.php');?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Add Account</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="dashboard.php">Home</a></li>
              <li class="breadcrumb-item active">Add Account</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Add Account</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" method="post" enctype="multipart/form-data">
                <div class="card-body">
                <div class="form-group">
                    <label for="exampleInputEmail1">PNR No.*</label>
                    <input type="text" class="form-control" id="pnr" name="pnr" value="<?php if (isset($_POST['pnr'])) echo $_POST['pnr']; ?>" placeholder="Enter PNR" required="true">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Customer ID</label>
                    <input type="text" class="form-control" id="cid" name="cid" value="<?php if (isset($_POST['cid'])) echo $_POST['cid']; ?>" placeholder="Enter Customer ID">
                    
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Account No.*</label>
                    <input type="text" class="form-control" id="AcNo" name="AcNo" placeholder="Enter Account Number" required="true" value="<?php if (isset($_POST['AcNo'])){ echo $_POST['AcNo'];}else{echo '5672021200';} ?>" maxlength="15">   
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Aadhar No.*</label>
                    <input type="text" class="form-control" id="AadharNo" value="<?php if (isset($_POST['AadharNo'])) echo $_POST['AadharNo']; ?>" name="AadharNo" placeholder="Enter aadhar No." maxlength="12"  required="true">
                  </div>
                     <div class="form-group">
                    <label for="exampleInputEmail1">Name*</label>
                    <input type="text" oninput="this.value = this.value.toUpperCase()" class="form-control" id="name" name="name" value="<?php if (isset($_POST['name'])) echo $_POST['name']; ?>" placeholder="Name" required="true">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Father's Name*</label>
                    <input type="text" oninput="this.value = this.value.toUpperCase()" oninput="this.value = this.value.toUpperCase()" class="form-control" id="FName" name="FName" value="<?php if (isset($_POST['FName'])) echo $_POST['FName']; ?>" placeholder="Enter Father's Name" required="true">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Mother's Name*</label>
                    <input type="text" oninput="this.value = this.value.toUpperCase()" class="form-control" id="MName" name="MName" value="<?php if (isset($_POST['MName'])) echo $_POST['MName']; ?>" placeholder="Enter Monther's  Name" required="true">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Marital Status*</label>
                      <select class="form-control" id="MStatus" name="MStatus" >
                        <option value="" disabled selected>Select Marital Status</option>
                        <option value="Unmarried">Unmarried</option>
                        <option value="Married">Married</option>
                      </select>
                  </div>
                  <div class="form-group" id="SNameDiv">
                    <label for="exampleInputEmail1">Spouse Name*</label>
                    <input type="text" oninput="this.value = this.value.toUpperCase()" class="form-control" id="SName" name="SName" value="<?php if (isset($_POST['SName'])) echo $_POST['SName']; ?>" placeholder="Enter Spouse Name" required="true">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Date Of Birth*</label>
                    <input type="date" class="form-control" id="DOB" name="DOB" value="<?php if (isset($_POST['DOB'])) echo $_POST['DOB']; ?>" placeholder="Enter D.O.B" required="true">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Gender*</label>
                    <select class="form-control" name="Gender" id="Gender">
                        <option value="" disabled selected>Select Gender</option>
                        <option value="MALE">Male</option>
                        <option value="FEMALE">Female</option>
                      </select>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Is Pan Available*</label>
                      <select class="form-control" id="PanStatus" name="PanStatus">
                        <option value="" disabled selected>Select Yes or No</option>
                        <option value="Yes">Yes</option>
                        <option value="No">No</option>
                      </select>
                  </div>
                  <div class="form-group" id="PanNoDiv">
                    <label for="exampleInputEmail1">Enter Pan Number*</label>
                    <input type="text" class="form-control" id="PanNo" value="<?php if (isset($_POST['PanNo'])) echo $_POST['PanNo']; ?>" name="PanNo" placeholder="Name" required="true">
                  </div>
                  <div class="form-group" id="PanFormDiv">
                    <label for="exampleInputEmail1">Form *</label>
                    <select class="form-control" id="PanForm" name="PanForm">
                        <option value="" disabled selected>Select Form</option>
                        <option value="Form-60">Form 60</option>
                        <option value="Form-61">Form 61</option>
                      </select>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Caste*</label>
                    <select class="form-control" name="caste" id="caste">
                        <option value="" disabled selected>Select Caste *</option>
                        <option value="General">General</option>
                        <option value="SC">SC</option>
                        <option value="ST">ST</option>
                        <option value="OBC">OBC</option>
                        <option value="Others">Others</option>
                      </select>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Mobile Number*</label>
                    <input type="text" class="form-control" id="mobilenumber" name="mobilenumber" value="<?php if (isset($_POST['mobilenumber'])) echo $_POST['mobilenumber']; ?>" placeholder="Mobile Number" maxlength="10" pattern="[0-9]+" required="true">
                  </div> 
                  <div class="form-group">
                    <label for="exampleInputEmail1">Address*</label>
                    <textarea type="text" oninput="this.value = this.value.toUpperCase()" class="form-control" id="address" name="address" value="<?php if (isset($_POST['address'])) echo $_POST['address']; ?>" placeholder="Address" required="true"></textarea>
                  </div> 
                  <div class="form-group">
                    <label for="exampleInputEmail1">Nominee's Name (optional)</label>
                    <input type="text" oninput="this.value = this.value.toUpperCase()" class="form-control" id="NName" name="NName" value="<?php if (isset($_POST['NName'])) echo $_POST['NName']; ?>" placeholder="Enter Nominee Name">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Nominee's Date Of Birth (optional)</label>
                    <input type="date" class="form-control" id="NDOB" name="NDOB" value="<?php if (isset($_POST['NDOB'])) echo $_POST['NDOB']; ?>" placeholder="Name" >
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Nominee's Address (optional)</label>
                    <textarea type="text" oninput="this.value = this.value.toUpperCase()" class="form-control" id="NAddress" name="NAddress"  value="<?php if (isset($_POST['NAddress'])) echo $_POST['NAddress']; ?>" placeholder="Address" ></textarea>
                  </div> 
                  <div class="form-group">
                    <label for="exampleInputEmail1">Profile Picture*</label>
                    <input type="file" class="form-control" id="propic" name="propic" value="<?php if (isset($_POST['propic'])) echo $_POST['propic']; ?>" required="true">
                  </div> 
                  <div class="form-group">
                    <label for="exampleInputEmail1">Signature*</label>
                    <input type="file" class="form-control" id="sigpic" name="sigpic" value="<?php if (isset($_POST['sigpic'])) echo $_POST['sigpic']; ?>" required="true">
                  </div>                  
                  <div class="form-group">
                    <label for="exampleInputEmail1">Debit card allotment*</label>
                    <select class="form-control" name="DebitAllot" id="DebitAllot" value="<?php if (isset($_POST['DebitAllot'])) echo $_POST['MName']; ?>" required="true">
                        <option value="" disabled selected>Select</option>
                        <option value="No">No</option>
                        <option value="Yes">Yes</option>
                      </select>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Passbook allotment*</label>
                    <select class="form-control" name="PassbookAllot" id="PassbookAllot" value="<?php if (isset($_POST['PassbookAllot'])) echo $_POST['PassbookAllot']; ?>" required="true">
                        <option value="" disabled selected>Select</option>
                        <option value="No">No</option>
                        <option value="Yes">Yes</option>
                      </select>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Account Opening Date *</label>
                    <input type="date" class="form-control" id="AccountOpeningDate" name="AccountOpeningDate" value="<?php if (isset($_POST['AccountOpeningDate'])) echo $_POST['AccountOpeningDate']; ?>" required="true">
                  </div>
                   
                </div>
              
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary" name="submit">Add Account</button>
                </div>
              </form>
            </div>
            <!-- /.card -->

          </div>
          <!--/.col (left) -->
          <!-- right column -->
         
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
 
<?php include_once('includes/footer.php');?>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- bs-custom-file-input -->
<script src="plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<script type="text/javascript">
$(document).ready(function () {
  bsCustomFileInput.init();
});
</script>
<script>
  $("#MStatus").change(function() {
  if ($(this).val() == "Married") {
    $('#SNameDiv').show();
    $('#SName').attr('required', '');
    $('#SName').attr('data-error', 'This field is required.');
  } else {
    $('#SNameDiv').hide();
    $('#SName').removeAttr('required');
    $('#SName').removeAttr('data-error');
  }
});
$("#MStatus").trigger("change");
</script>
<script>
   $("#PanStatus").change(function() {
  if ($(this).val() == "Yes") {
    $('#PanNoDiv').show();
    $('#PanNo').attr('required', '');
    $('#PanNo').attr('data-error', 'This field is required.');
    $('#PanFormDiv').hide();
    $('#PanForm').removeAttr('required');
    $('#PanForm').removeAttr('data-error');
  } else if($(this).val() == "No"){
    $('#PanFormDiv').show();
    $('#PanForm').attr('required', '');
    $('#PanForm').attr('data-error', 'This field is required.');
    $('#PanNoDiv').hide();
    $('#PanNo').removeAttr('required');
    $('#PanNo').removeAttr('data-error');
  }else{
    $('#PanNoDiv').hide();
    $('#PanNo').removeAttr('required');
    $('#PanNo').removeAttr('data-error');
    $('#PanFormDiv').hide();
    $('#PanForm').removeAttr('required');
    $('#PanForm').removeAttr('data-error');
  }
});
$("#PanStatus").trigger("change");
</script>
</body>
</html>
<?php }  ?>