<?php
session_start();
error_reporting(0);
include('includes/dbconnection.php');
if (strlen($_SESSION['brmsaid']==0)) {
  header('location:logout.php');
  } else{
   
  ?>
<!DOCTYPE html>
<html>
<head>
  
  <title>BRMS | Banking Resource Management System</title>
    
  <!-- Font Awesome -->
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <style>
.tableMine, th, td {
  border: 1px solid black;
  border-collapse: collapse;
  
 
}
th, td {
  padding: 15px;
  text-align: left;
}

</style>
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <?php include_once('includes/header.php');?>

 
<?php include_once('includes/sidebar.php');?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Verify</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="dashboard.php">Home</a></li>
              <li class="breadcrumb-item active">Update Person Detail</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
                <?php
                   $eid=$_GET['editid'];
$sql="SELECT * from tblperson where PassbookNo=$eid";
$query = $dbh -> prepare($sql);
$query->execute();
$results=$query->fetchAll(PDO::FETCH_OBJ);


if($query->rowCount() > 0)
{
foreach($results as $row)
{               ?>
<table border="1" class="table table-bordered" > 
<td colspan="6" style="font-size:20px;color:blue">
 Passbook No :  <?php  echo ($row->PassbookNo);?></td></tr>
</table>
<table border="" class="tableMine" style="width:100%" > 
    <tr>
    <th scope>Full Name :</th>
    <td><?php  echo ($row->Name);?></td>
    <th scope>Account No. :</th>
    <td><?php  echo ($row->AccountNo);?></td>
  </tr>
<tr>
    <th scope>Customer ID :</th>
    <td><?php  echo ($row->CID);?></td>
    <th scope>Aadhar No. :</th>
    <td><?php  echo ($row->AadharNo);?></td>

  </tr>
  <tr>
    <th scope>Mobile Number :</th>
    <td colspan="3"><?php  echo ($row->MobileNumber);?></td>
  </tr>
  <tr>
    <th scope>Address :</th>
    <td colspan="3"><?php  echo ($row->Address);?></td>
  </tr>
<tr>
    <th scope>Image :</th>
    <td><img src="images/<?php echo $row->Picture;?>" width="100" height="100" value="<?php  echo $row->Picture;?>"></td>
    <th scope>Signature :</th>
    <td> <img src="images/<?php echo $row->Signature;?>" width="100" height="100" value="<?php  echo $row->Signature;?>"></td>
  </tr>
                                    
   <?php }} ?>
   </table>
                <div class="card-footer">
                  <a href="list-account.php" class="btn btn-primary" name="submit" align=>Go Back</a>
                </div>
            </div>
            <!-- /.card -->

          </div>
          <!--/.col (left) -->
          <!-- right column -->
         
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
 
<?php include_once('includes/footer.php');?>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- bs-custom-file-input -->
<script src="plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<script type="text/javascript">
$(document).ready(function () {
  bsCustomFileInput.init();
});
</script>

</body>
</html>
<?php }  ?>