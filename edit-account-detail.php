<?php
session_start();
error_reporting(0);
include('includes/dbconnection.php');
if (strlen($_SESSION['brmsaid']==0)) {
  header('location:logout.php');
  } else{
    if(isset($_POST['submit']))
  {
$eid=$_GET['editid'];
$name=$_POST['name'];
$PanStatus=$_POST['PanStatus'];
$PanNo=$_POST['PanNo'];
$PanForm=$_POST['PanForm'];
$FName=$_POST['FName'];
$caste=$_POST['caste'];
$MName=$_POST['MName'];
$MStatus=$_POST['MStatus'];
$SName=$_POST['SName'];
$DOB=date('Y-m-d', strtotime($_POST['DOB']));
$NDOB=date('Y-m-d', strtotime($_POST['NDOB']));
$AccountOpeningDate=date('Y-m-d', strtotime($_POST['AccountOpeningDate']));
$DebitAllot=$_POST['DebitAllot'];
$PassbookAllot=$_POST['PassbookAllot'];
$Gender=$_POST['Gender'];
$NName=$_POST['NName'];
$NAddress=$_POST['NAddress'];
$mobnum=$_POST['mobilenumber'];
$address=$_POST['address'];
$cid=$_POST['cid'];
$pnr=$_POST['pnr'];
$AadharNo=$_POST['AadharNo'];
$AcNo=$_POST['AcNo'];



$sql="update tblperson set PNR='$pnr',CID='$cid',AccountNo='$AcNo',AadharNo='$AadharNo',Name='$name',FName='$FName',MName='$MName',MStatus='$MStatus',SName='$SName',DOB='$DOB',Gender='$Gender',PanStatus='$PanStatus',PanNo='$PanNo',PanForm='$PanForm',caste='$caste',MobileNumber='$mobnum',Address='$address',NName='$NName',NDOB='$NDOB',NAddress='$NAddress',DebitAllot='$DebitAllot',PassbookAllot='$PassbookAllot' where PassbookNo='$eid'";
                       if($conn->query($sql)===TRUE){
                        echo '<script>alert("Person Detail has been edited successfully.")</script>';
                        }
  else
    {
         echo "Error: " . $sql . "<br>" . $conn->error;
    }
  
  }
  ?>
<!DOCTYPE html>
<html>
<head>
  
  <title>BRMS | Banking Resource Management System</title>
    
  <!-- Font Awesome -->
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <?php include_once('includes/header.php');?>

 
<?php include_once('includes/sidebar.php');?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Add Person</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="dashboard.php">Home</a></li>
              <li class="breadcrumb-item active">Update Person Detail</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Update Person Detail</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" method="post" enctype="multipart/form-data">
                <?php
                   $eid=$_GET['editid'];
$sql="SELECT * from tblperson where PassbookNo=$eid";
$query = $dbh -> prepare($sql);
$query->execute();
$results=$query->fetchAll(PDO::FETCH_OBJ);

$cnt=1;
if($query->rowCount() > 0)
{
foreach($results as $row)
{               ?>
                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1">PNR No.</label>
                    <input type="text" class="form-control" id="pnr" name="pnr" value="<?php echo htmlentities($row->PNR);?>" required="true">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Customer ID</label>
                    <input type="text" class="form-control" id="cid" name="cid" value="<?php echo htmlentities($row->CID);?>">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Account No</label>
                    <input type="text" class="form-control" id="AcNo" name="AcNo" value="<?php echo htmlentities($row->AccountNo);?>" required="true">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Aadhar No</label>
                    <input type="text" class="form-control" id="AadharNo" name="AadharNo" value="<?php echo htmlentities($row->AadharNo);?>" required="true">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Name</label>
                    <input oninput="this.value = this.value.toUpperCase()" type="text" class="form-control" id="name" name="name" value="<?php echo htmlentities($row->Name);?>" required="true" style="text-transform:uppercase">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Father's Name</label>
                    <input oninput="this.value = this.value.toUpperCase()" type="text" class="form-control" id="FName" name="FName" value="<?php echo htmlentities($row->FName);?>" required="true" style="text-transform:uppercase">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Mother's Name</label>
                    <input oninput="this.value = this.value.toUpperCase()" type="text" class="form-control" id="MName" name="MName" value="<?php echo htmlentities($row->MName);?>" required="true" style="text-transform:uppercase">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Marital Status</label>
                    <select class="form-control" name="MStatus" id="MStatus" >
                    <option value="<?php  echo $row->MStatus;?>"><?php  echo $row->MStatus;?></option>
                    <option value="Unmarried">Unmarried</option>
                    <option value="Married">Married</option>
                  </select>
                  </div> 
                  <div class="form-group" id="SNameDiv">
                    <label for="exampleInputEmail1">Spouse Name</label>
                    <input oninput="this.value = this.value.toUpperCase()" type="text" class="form-control" id="SName" name="SName" value="<?php echo htmlentities($row->SName);?>" required="true" placeholder="null" style="text-transform:uppercase">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Date Of Birth</label>
                    <input type="date" class="form-control" id="DOB" name="DOB" value="<?php echo htmlentities($row->DOB);?>" required="true">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Gender</label>
                    <select class="form-control" name="Gender" id="Gender">
                    <option value="<?php  echo $row->Gender;?>"><?php  echo $row->Gender;?></option>
                    <option value="Male">Male</option>
                    <option value="Female">Female</option> 
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Is Pan Available</label>
                    <select  name="PanStatus" id="PanStatus"  class="form-control" required='true'>
                    <option value="<?php  echo $row->PanStatus;?>"><?php  echo $row->PanStatus;?></option>
                    <option value="Yes">Yes</option>
                    <option value="No">No</option>
                  </select>
                  </div> 
                     
                  <div class="form-group" id="PanNoDiv"> 
                    <label for="exampleInputEmail1">Pan Card No.</label>
                    <input type="text" class="form-control" id="PanNo" name="PanNo" value="<?php echo htmlentities($row->PanNo);?>" required="true">
                  </div>  
                  <div class="form-group" id="PanFormDiv">
                    <label for="exampleInputEmail1">Pan Card Form</label>
                    <select name="PanForm" id="PanForm" class="form-control" >
                    <option value="<?php  echo $row->PanForm;?>"><?php  echo $row->PanForm;?></option>
                    <option value="Form-60">Form-60</option>
                    <option value="Form-61">Form-61</option>
                  </select>
                  </div> 
                  <div class="form-group">
                    <label for="exampleInputEmail1">Caste</label>
                    <select class="form-control" name="caste" id="caste">
                        <option value="<?php  echo $row->caste;?>" ><?php  echo $row->caste;?></option>
                        <option value="General">General</option>
                        <option value="SC">SC</option>
                        <option value="ST">ST</option>
                        <option value="OBC">OBC</option>
                        <option value="Others">Others</option>
                      </select>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Mobile Number</label>
                    <input type="text" class="form-control" id="mobilenumber" name="mobilenumber" value="<?php echo htmlentities($row->MobileNumber);?>" required="true">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Address</label>
                    <textarea oninput="this.value = this.value.toUpperCase()" type="text" class="form-control" id="address" name="address" placeholder="Address" required="true" style="text-transform:uppercase"><?php echo htmlentities($row->Address);?></textarea>
                  </div> 
                  <div class="form-group">
                    <label for="exampleInputEmail1">Nominee's Name</label>
                    <input oninput="this.value = this.value.toUpperCase()" type="text" class="form-control" id="NName" name="NName" value="<?php echo htmlentities($row->NName);?>" style="text-transform:uppercase">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Nominee's DOB</label>
                    <input type="date" class="form-control" id="NDOB" name="NDOB" value="<?php echo htmlentities($row->NDOB);?>" >
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Nominee's Address</label>
                    <textarea oninput="this.value = this.value.toUpperCase()" type="text" class="form-control" id="NAddress" name="NAddress" placeholder="Address" style="text-transform:uppercase"><?php echo htmlentities($row->NAddress);?></textarea>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Profile Picture : </label>
                    <img src="images/<?php echo $row->Picture;?>" width="100" height="100" value="<?php  echo $row->Picture;?>" >
                    <a href="changeimage.php?editid=<?php echo $row->PassbookNo;?>"> &nbsp; Edit Image</a>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Signature : </label>
                    <img src="images/<?php echo $row->Signature;?>" width="100" height="100" value="<?php  echo $row->Signature;?>">
                    <a href="changesignature.php?editid=<?php echo $row->PassbookNo;?>"> &nbsp; Edit Image</a>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Debit Card Alloted?</label>
                    <select type="text" name="DebitAllot" id="DebitAllot" value="" class="form-control" required='true'>
                    <option value="<?php  echo $row->DebitAllot;?>"><?php  echo $row->DebitAllot;?></option>
                    <option value="No">No</option>
                    <option value="Yes">Yes</option>
                  </select>
                  </div> 
                  <div class="form-group">
                    <label for="exampleInputEmail1">Passbook Alloted?</label>
                    <select type="text" name="PassbookAllot" id="PassbookAllot" value="" class="form-control" required='true'>
                    <option value="<?php  echo $row->PassbookAllot;?>"><?php  echo $row->PassbookAllot;?></option>
                    <option value="No">No</option>
                     <option value="Yes">Yes</option>
                  </select>
                  </div> 
                  <div class="form-group">
                    <label for="exampleInputEmail1">Account Opening Date</label>
                    <input type="date" class="form-control" id="AccountOpeningDate" name="AccountOpeningDate" value="<?php echo htmlentities($row->AccountOpeningDate);?>" required="true">
                  </div> 
                </div>
                <?php $cnt=$cnt+1;}} ?> 
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary" name="submit">Update Details</button>
                </div>
              </form>
            </div>
            <!-- /.card -->

          </div>
          <!--/.col (left) -->
          <!-- right column -->
         
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
 
<?php include_once('includes/footer.php');?>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- bs-custom-file-input -->
<script src="plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<script>
  $("#MStatus").change(function() {
  if ($(this).val() == "Married") {
    $('#SNameDiv').show();
    $('#SName').attr('required', '');
    $('#SName').attr('data-error', 'This field is required.');
  } else {
    $('#SNameDiv').hide();
    $('#SName').removeAttr('required');
    $('#SName').removeAttr('data-error');
  }
});
$("#MStatus").trigger("change");
</script>
<script>
   $("#PanStatus").change(function() {
  if ($(this).val() == "Yes") {
    $('#PanNoDiv').show();
    $('#PanNo').attr('required', '');
    $('#PanNo').attr('data-error', 'This field is required.');
    $('#PanFormDiv').hide();
    $('#PanForm').removeAttr('required');
    $('#PanForm').removeAttr('data-error');
  } else if($(this).val() == "No"){
    $('#PanFormDiv').show();
    $('#PanForm').attr('required', '');
    $('#PanForm').attr('data-error', 'This field is required.');
    $('#PanNoDiv').hide();
    $('#PanNo').removeAttr('required');
    $('#PanNo').removeAttr('data-error');
  }else{
    $('#PanNoDiv').hide();
    $('#PanNo').removeAttr('required');
    $('#PanNo').removeAttr('data-error');
    $('#PanFormDiv').hide();
    $('#PanForm').removeAttr('required');
    $('#PanForm').removeAttr('data-error');
  }
});
$("#PanStatus").trigger("change");
</script> 
<script type="text/javascript">
$(document).ready(function () {
  bsCustomFileInput.init();
});
</script>

</body>
</html>
<?php }  ?>